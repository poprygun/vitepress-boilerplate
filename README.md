# Vitpress boilerplate

Ready to work multilingual vitepress boilerplate with autogenerated sidebar.

## Getting started
```
cd docs
git clone https://gitlab.com/its.bz/vitpress-boilerplate.git ./
yarn install
```

## Usage
```shell
yarn dev
yarn build
```
Built documentation can be found in `pubic` directory.

See the [demo](https://poprygun.gitlab.io/vitepress-boilerplate/en/)

## Contributing
Contributors are welcome!

## License
MIT
