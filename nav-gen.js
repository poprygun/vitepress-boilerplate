const fs = require('fs')
const path = require('path')
const walk = (dir, breadcrumbs, title) => {
    let res = []
    const text = getTitle(path.join(dir, 'index.md'))
    const parent = {text, link: `${breadcrumbs}/index.html`, collapsible: true, children: []}
    const list = fs.readdirSync(dir)
    const items = list.filter(i => !['.vitepress', '.vuepress'].includes(i))
    let pending = items.length
    if (!pending) return res
    items.forEach((item) => {
        const itemName = item
        const itemPath = path.resolve(dir, item)
        const stat = fs.statSync(itemPath)
        if (stat && stat.isDirectory()) {
            const childs = walk(itemPath, `${breadcrumbs}/${itemName}`, itemName)
            parent.children.push(...childs)
        } else {
            if (itemName !== 'index.md') {
                const url = itemName.substr(0, itemName.length - 3) + '.html'
                const text = getTitle(itemPath)
                parent.children.splice(0, 0, {
                    text, link: `${breadcrumbs}/${url}`
                })
            }
        }
    })
    res.push(parent)
    return res
}

const getTitle = (file) => {
    try {
        const f = fs.readFileSync(file).toString()
        return f.match(/^# (.*)/)[1] || '###'
    } catch (e) {
        return ''
    }
}

const obj = walk(path.join(__dirname, 'docs/en'), '/en', 'Home')
const tree = JSON.stringify(obj[0].children, null, 2)
fs.writeFileSync(path.join(__dirname, 'docs', '.vitepress', 'sidebar.json'), tree)

const lng = walk(path.join(__dirname, 'docs/ru'), '/ru', 'Home')
const treelng = JSON.stringify(lng[0].children, null, 2)
fs.writeFileSync(path.join(__dirname, 'docs', '.vitepress', `sidebar.ru.json`), treelng)

console.log('DONE!')
