# Changelog

```
1.0.0 @ 27 January 2022  
- Initial commit  
- Initial commit  
- Add Gitlab pages CI  
- Fix Gitlab pages deploy CI  
- Fix Gitlab pages deploy CI  
- Add Gitlab pages CI  
- Add Gitlab pages CI  
- Add Gitlab pages CI

```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

MIT
