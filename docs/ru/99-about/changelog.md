# История изменений

```
1.0.0 @ 27 January 2022  
- Initial commit  
- Initial commit  
- Add Gitlab pages CI  
- Fix Gitlab pages deploy CI  
- Fix Gitlab pages deploy CI  
- Add Gitlab pages CI  
- Add Gitlab pages CI  
- Add Gitlab pages CI

```

## Контакты

Свяжитесь с нами: [dev@its.bz](mailto:dev@its.bz)\
Наш сайт: [its.bz](https://its.bz)

## Лицензия

MIT
