# Changelog

```
{{#each releases}}{{title}} @ {{niceDate}}{{#commits}}  
  - {{subject}}{{/commits}}

{{/each}}
```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

MIT
